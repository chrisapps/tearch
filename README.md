# Tearch

_A PHP string-search library supporting common string-search algorithms._

This PHP library deals with string-searching for arbitrary-length texts using common string-search algorithms like Boyer-Moore, Rabin-Karp or Knuth-Morris-Pratt ect. Search results provide useful information like start and end indices of any occurence of the string-search pattern as well as elapsed time needed for the entire performance to find a matching text fragment in the input text.

